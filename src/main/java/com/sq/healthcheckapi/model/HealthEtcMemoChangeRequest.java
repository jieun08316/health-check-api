package com.sq.healthcheckapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthEtcMemoChangeRequest  {
    private String etcMemo;
}
