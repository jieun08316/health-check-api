package com.sq.healthcheckapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthBaseInfoChangeRequest {

    private String name;

    private Boolean isChronicDisease;
}
